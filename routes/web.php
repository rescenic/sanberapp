<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/master', function(){
    return view('index');
});

Route::get('/table', function(){
    return view('pages.table');
});

Route::get('/data-table', function(){
    return view('pages.data-table');
});

//Tugas CRUD Cast
//CREATE Data
//Route untuk mengarahkan ke form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);

//STORE Data
//Route untuk menyimpan data baru ke tabel cast
Route::post('/cast', [CastController::class, 'store']);

//READ Data
Route::get('/cast', [CastController::class, 'index']);
//Menampilkan List Data Para Pemain Film
// Route::get('/cast', function(){
//     return 'Berhasil Insert Data';
// });

//Detail Cast Berdasarkan ID
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//Edit Cast Berdasarkan ID
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

//Update Cast Berdasarkan ID
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete Cast Berdasarkan ID
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);


