<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('pages.register');
    }

    public function welcome(Request $request){
        //dd($request->all());

        $fullname = $request['fname'] . " " . $request['lname'];

        return view('pages.welcome', ['fullname' => $fullname]);
    }
}
