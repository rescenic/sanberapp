<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {
        //dd($request->all());

        $request->validate([
            'namacast' => 'required|min:5',
            'umurcast' => 'required',
            'biocast' => 'required',
        ], [
            'namacast.required' => "Nama Cast harus diisi tidak boleh kosong",
            'umurcast.required' => "Umur Cast harus diisi tidak boleh kosong",
            'biocast.required' => "Bio Cast harus diisi tidak boleh kosong",
            'namacast.min' => "Nama Cast harus minimal 5 karakter",
        ]);

        DB::table('cast')->insert([
            'nama' => $request['namacast'],
            'umur' => $request['umurcast'],
            'bio' => $request['biocast'],
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get(); //SELECT * FROM cast;
        //dd($cast);
        return view('cast.tampil', ['cast'=>$cast]);
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.detail', ['cast'=>$cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', ['cast'=>$cast]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'namacast' => 'required|min:5',
            'umurcast' => 'required',
            'biocast' => 'required',
        ], [
            'namacast.required' => "Nama Cast harus diisi tidak boleh kosong",
            'umurcast.required' => "Umur Cast harus diisi tidak boleh kosong",
            'biocast.required' => "Bio Cast harus diisi tidak boleh kosong",
            'namacast.min' => "Nama Cast harus minimal 5 karakter",
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['namacast'],
                    'umur' => $request['umurcast'],
                    'bio' => $request['biocast'],
                ]
            );

        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=' ,$id)->delete();

        return redirect('/cast');

    }
}
