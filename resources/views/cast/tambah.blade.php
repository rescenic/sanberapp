@extends('layout.master')

@section('title')
    Tambah Cast Pemain Baru
@endsection

@section('content')
    <h3>Formulir Cast</h3>

    <form action="/cast" method="POST">
        @csrf

        <div class="form-group">
            <label for="namacast_label">Nama Cast:</label>
            <input name="namacast" type="text" class="form-control" value="{{old('namacast')}}">
        </div>
        @error('namacast')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="umurcast_label">Umur Cast:</label>
            <input name="umurcast" type="number" class="form-control" value="{{old('umurcast')}}">
        </div>
        @error('umurcast')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="biocast_label">Bio Cast:</label>
            <textarea name="biocast" class="form-control" rows="3">{{old('biocast')}}</textarea>
        </div>
        @error('biocast')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <input type="submit" value="Submit" />
    </form>
@endsection
