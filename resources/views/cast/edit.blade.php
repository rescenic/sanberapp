@extends('layout.master')

@section('title')
    Edit Cast Pemain Baru
@endsection

@section('content')
    <h3>Formulir Cast</h3>

    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')

        <div class="form-group">
            <label for="namacast_label">Nama Cast:</label>
            <input name="namacast" type="text" class="form-control" value="{{old('namacast', $cast->nama)}}">
        </div>
        @error('namacast')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="umurcast_label">Umur Cast:</label>
            <input name="umurcast" type="number" class="form-control" value="{{old('umurcast', $cast->umur)}}">
        </div>
        @error('umurcast')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="biocast_label">Bio Cast:</label>
            <textarea name="biocast" class="form-control" rows="3">{{old('biocast', $cast->bio)}}</textarea>
        </div>
        @error('biocast')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <input type="submit" value="Submit" />
    </form>
@endsection
