@extends('layout.master')

@section('title')
    Tampilan Detail Cast Pemain
@endsection

@section('content')
<h1 class="text-primary">{{$cast->nama}}</h1>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>
@endsection
