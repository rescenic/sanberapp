@extends('layout.master')

@section('title')
    Buat Account Baru!
@endsection

@section('content')
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf

        <label>First name:</label> <br />
        <input type="text" name="fname" /> <br /><br />

        <label>Last name:</label> <br />
        <input type="text" name="lname" /> <br /><br /><br />

        <label>Gender:</label><br />
        <input type="radio" name="gender" value="1" />Male <br />
        <input type="radio" name="gender" value="2" />Female <br /><br />

        <label>Nationality:</label><br />
        <select name="nationality" id="">
            <option value="1">Indonesian</option>
            <br />
            <option value="2">Japanese</option>
            <br />
            <option value="3">Kenyan</option>
            <br />
            <option value="4">Norwegian</option>
            <br />
        </select>

        <br /><br />

        <label>Language Spoken:</label><br />
        <input type="checkbox" value="1" name="lang" />Bahasa Indonesia
        <br />
        <input type="checkbox" value="2" name="lang" />English <br />
        <input type="checkbox" value="3" name="lang" />Other

        <br /><br />

        <label>Bio:</label><br />
        <textarea name="bio" cols="30" rows="10"></textarea><br /><br />

        <input type="submit" value="Submit" />
    </form>
@endsection
